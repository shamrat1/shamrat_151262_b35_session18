<?php
class StudentInfo{
    public function doSomething(){
        echo "hello world";
    }
    public function __call($name, $arguments)
    {
        echo $name.'<br>';// it calls itself when a method doesn't exists but it is called
    }
    public static function __callStatic($name, $arguments)
    {
        echo $name.'<br>';
    }
}

$myObj= new StudentInfo();
$myObj->doSomething();

StudentInfo::wrongStatic();