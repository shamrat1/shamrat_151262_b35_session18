<?php
class ImClass{
    public $a = 1;
    public $b = 2;
    public $c = 3;
    public $d = 4;
    public function __construct($value)
    {
        echo $value."<br>";
    }
    public static function __callStatic($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
        echo "<br>";
    }
    public function __isset($name)
    {
        echo $name."<br>";
    }
    public function __unset($name)
    {
        echo $name."<br>";
    }
    public function __sleep()
    {
        echo "i'm inside sleep magic method<br>";
        return array('a','c');
    }

    public function __destruct()
    {
        echo "I'm inside __destruct.<br>";
    }
}
ImClass::messageMe(65,45,"helloooooo");
$obj = new ImClass("i am inside construct magic method");
isset($obj->myProperty);
unset($obj->unsettingProperty);
$test = serialize($obj);
echo $test;
echo "Hello there";
