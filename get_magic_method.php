<?php
class StudentInfo{
    private $myVar = "php";

   public function __get($name)
    {
        echo $name;
    }
    public function __set($name, $value)
    {
        echo $name;
    }
    public function __isset($name)
    {
        echo $name;
    }
    public function __unset($name)
    {
        echo $name;
    }
}
$obj = new StudentInfo();
//$obj->myVar = "Lambo";
//http://culttt.com/2014/04/16/php-magic-methods/
//$result = isset($obj->myVar);

//echo $obj->myVar;

 unset($obj->myVar);
class TestClass
{
    public $foo;
    public function __construct($foo)
    {
        $this->foo = $foo;
    }

    public function __toString()
    {
        return $this->foo;
    }
}
$tweet = new TestClass('<br>Hello world');

echo $tweet;